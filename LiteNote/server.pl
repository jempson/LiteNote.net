use 5.010;
use strict;
use warnings;
use Win32::Pipe;
use Text::Markdown 'markdown';
use Encode;
use Unicode::Collate::Locale;
#binmode(STDOUT, ':encoding(gb2312)');  #这里是关键

sub main;
sub mark;

my $pre = '<html>
<head>
<style id="system" type="text/css">
*
{
margin: 0;
padding: 0;
}
body
{
font: 13.34px helvetica,arial,freesans,clean,sans-serif;
color: black;
line-height: 1.4em;
background-color: #F8F8F8;
padding: 0.7em;
}
p
{
margin: 1em 0;
line-height: 1.5em;
}
table
{
font-size: inherit;
font: 100%;
margin: 1em;
}
table th
{
border-bottom: 1px solid #bbb;
padding: .2em 1em;
}
table td
{
border-bottom: 1px solid #ddd;
padding: .2em 1em;
}
input[type=text], input[type=password], input[type=image], textarea
{
font: 99% helvetica,arial,freesans,sans-serif;
}
select, option
{
padding: 0 .25em;
}
optgroup
{
margin-top: .5em;
}
pre, code
{
font: 12px Monaco, "Courier New" , "DejaVu Sans Mono" , "Bitstream Vera Sans Mono" ,monospace;
}
pre
{
margin: 1em 0;
font-size: 12px;
background-color: #eee;
border: 1px solid #ddd;
padding: 5px;
line-height: 1.5em;
color: #444;
overflow: auto;
-webkit-box-shadow: rgba(0,0,0,0.07) 0 1px 2px inset;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
white-space: pre-wrap;
word-wrap: break-word;
}
pre code
{
padding: 0;
font-size: 12px;
background-color: #eee;
border: none;
}
code
{
font-size: 12px;
background-color: #f8f8ff;
color: #444;
padding: 0 .2em;
border: 1px solid #dedede;
}
img
{
border: 0;
max-width: 100%;
}
abbr
{
border-bottom: none;
}
a
{
color: #4183c4;
text-decoration: none;
}
a:hover
{
text-decoration: underline;
}
a code, a:link code, a:visited code
{
color: #4183c4;
}
h2, h3
{
margin: 1em 0;
}
h1, h2, h3, h4, h5, h6
{
border: 0;
}
h1
{
font-size: 170%;
border-top: 4px solid #aaa;
padding-top: .5em;
margin-top: 1.5em;
}
h1:first-child
{
margin-top: 0;
padding-top: .25em;
border-top: none;
}
h2
{
font-size: 150%;
margin-top: 1.5em;
border-top: 4px solid #e0e0e0;
padding-top: .5em;
}
h3
{
margin-top: 1em;
}
hr
{
border: 1px solid #ddd;
}
ul
{
margin: 1em 0 1em 2em;
}
ol
{
margin: 1em 0 1em 2em;
}
ul li, ol li
{
margin-top: .5em;
margin-bottom: .5em;
}
ul ul, ul ol, ol ol, ol ul
{
margin-top: 0;
margin-bottom: 0;
}
blockquote
{
margin: 1em 0;
border-left: 5px solid #ddd;
padding-left: .6em;
color: #555;
}
dt
{
font-weight: bold;
margin-left: 1em;
}
dd
{
margin-left: 2em;
margin-bottom: 1em;
}

</style>
</head>
<body>';

my $next = '</body>
</html>';

main;

sub mark
{
#my $frm = "utf32";
    my $frm = "gb2312";

    while (1) {
        my $pipe = Win32::Pipe->new("markdown");

        $pipe->Connect();

        my $len = Encode::decode($frm, $pipe->Read());
        my $read_len = 0;
        my $txt;

        $pipe->Write("ReadLenEnd"); #隔离两个read


        say "len: " . $len;


        while (1) {
            my $temp = $pipe->Read();

            $txt .= $temp;
            $read_len += length($temp);

#say "read_len:" . $read_len;

            if ($read_len == $len) {
                last;
            }
        }

        if ($len == 8 && $txt eq "00000000") {
            return;
        }

        $txt = Encode::decode($frm, $txt);

#say "========Text" . Encode::encode("gb2312", $txt);


        my $html = markdown($txt);
        $html = $pre . $html . $next;


        my $c = Encode::encode($frm, $html);

        $len = length($c);
        my $index = 0;
        my $one = 500;

#$pipe->Write($c); #512个字符的限制，要分开来传输
        $pipe->Write($len);

        my $temp = $pipe->Read();  #在两个write之间要有一个read来隔离

        while (1) {
            if ($index + $one > $len) {
                $pipe->Write(substr($c, $index));
                last;
            } else {
                $pipe->Write(substr($c, $index, $one));
            }
            $index += $one;
        }

        $pipe->Disconnect();
        say "End_Discount\n\n";
    }

}

sub main
{
    mark;  
    #say $pre;
    #&markdown; 

#my $data = $pipe->Read();
#say $data;
}

